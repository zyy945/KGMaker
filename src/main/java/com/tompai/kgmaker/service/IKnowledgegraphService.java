/** 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tompai.kgmaker.service;

import java.util.List;
import java.util.Map;

public interface IKnowledgegraphService {
	List<Map<String,Object>> getDomains();
	List<Map<String,Object>> getDomainList(String domainname,String createuser);
	void saveDomain(Map<String, Object> map);
	void updateDomain(Map<String, Object> map);
	void deleteDomain(Integer id);
	List<Map<String,Object>> getDomainByName(String domainname);
	List<Map<String,Object>> getDomainById(Integer domainid);
	void saveNodeImage(List<Map<String, Object>> mapList);
	void saveNodeContent(Map<String, Object> map);
	void updateNodeContent(Map<String, Object> map);
	List<Map<String,Object>> getNodeImageList(Integer domainid,Integer nodeid);
	List<Map<String,Object>> getNodeContent(Integer domainid,Integer nodeid);
	void deleteNodeImage(Integer domainid,Integer nodeid);
}
