/** 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tompai.kgmaker.util;

import java.io.File;
import java.io.IOException;

import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.MultipartFile;

import com.tompai.kgmaker.controller.BaseController;


@Configuration
public class UploadService extends BaseController{
	
	@Value("${image.location}")
	private String realPath;
    //@Bean
    public String uploadImage(MultipartFile image,String fileName) throws FileUploadException {
    	
    	if (image!=null) {// 判断上传的文件是否为空
            String path=null;// 文件路径
            String type=null;// 文件类型
            String imageName=image.getOriginalFilename();// 文件原名称
            System.out.println("上传的文件原名称:"+fileName);
            // 判断文件类型
            type=fileName.indexOf(".")!=-1?fileName.substring(fileName.lastIndexOf(".")+1, fileName.length()):null;
            if (type!=null) {// 判断文件类型是否为空
                if ("GIF".equals(type.toUpperCase())||"PNG".equals(type.toUpperCase())||"JPG".equals(type.toUpperCase())) {
                    // 项目在容器中实际发布运行的根路径
                    //String realPath=request.getSession().getServletContext().getRealPath("/");
                    // 自定义的文件名称
                    String trueFileName=String.valueOf(System.currentTimeMillis())+imageName;
                    // 设置存放图片文件的路径
                    path=realPath+/*System.getProperty("file.separator")+*/trueFileName;
                    log.info("存放图片文件的路径:"+path);
                    // 转存文件到指定的路径
                    try {
						image.transferTo(new File(path));
					} catch (IllegalStateException e) {
						//TODO Auto-generated catch block
						e.printStackTrace();
						 return null;
					} catch (IOException e) {
						//TODO Auto-generated catch block
						e.printStackTrace();
						 return null;
					}
                    log.info("文件成功上传到指定目录下");
                    return path;
                }else {
                	 log.info("不是我们想要的文件类型,请按要求重新上传");
                    return null;
                }
            }else {
            	 log.info("文件类型为空");
                return null;
            }
        }else {
        	 log.info("没有找到相对应的文件");
            return null;
        }
    }
}
