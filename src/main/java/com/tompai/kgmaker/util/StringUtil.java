/** 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tompai.kgmaker.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ObjectUtils;

public class StringUtil {
	 /*
	 * 是否为空字符串
     * @param str
     * @return
     */
    public static boolean isBlank(String str){
        int strLen;
        if (str == null || (strLen = str.length()) == 0) {
            return true;
        }
        for (int i = 0; i < strLen; i++) {
            if (Character.isWhitespace(str.charAt(i)) == false) {
                return false;
            }
        }
        return true;
    }

    public static boolean isNotBlank(String str){
        return !isBlank(str);
    }
    /**
     * 连接方法 类似于javascript
     * @param join 连接字符串
     * @param strAry 需要连接的集合
     * @return
     */
    public static String join(String join,String[] strAry){
        StringBuffer sb=new StringBuffer();
        for(int i=0,len =strAry.length;i<len;i++){
            if(i==(len-1)){
                sb.append(strAry[i]);
            }else{
                sb.append(strAry[i]).append(join);
            }
        }
        return sb.toString();
    }

    /**
     * 将结果集中的一列用指定字符连接起来
     * @param join 指定字符
     * @param cols 结果集
     * @param colName 列名
     * @return
     */
    public static String join(String join,List<Map> cols,String colName){
        List<String> aColCons = new ArrayList<String>();
        for (Map map:
             cols) {
            aColCons.add(ObjectUtils.toString(map.get(colName)));
        }
        return join(join,aColCons);
    }

    public static String join(String join,List<String> listStr){
        StringBuffer sb=new StringBuffer();
        for(int i=0,len =listStr.size();i<len;i++){
            if(i==(len-1)){
                sb.append(listStr.get(i));
            }else{
                sb.append(listStr.get(i)).append(join);
            }
        }
        return sb.toString();
    }
}
