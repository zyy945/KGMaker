/** 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tompai.kgmaker.util;

import java.io.File;

import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.springframework.web.multipart.MultipartFile;

public interface UploadUtil {
	String uploadFile(MultipartFile multipartFile) throws FileUploadException;

	String uploadFile(String filePath, MultipartFile multipartFile) throws FileUploadException;

	String uploadFile(MultipartFile multipartFile, String fileName) throws FileUploadException;

	String uploadFile(MultipartFile multipartFile, String fileName, String filePath) throws FileUploadException;

	String uploadFile(File file) throws FileUploadException;

	String uploadFile(String filePath, File file) throws FileUploadException;

	String uploadFile(File file, String fileName) throws FileUploadException;

	String uploadFile(File file, String fileName, String filePath) throws FileUploadException;

	String uploadFile(byte[] data) throws FileUploadException;

	String uploadFile(String filePath, byte[] data) throws FileUploadException;

	String uploadFile(byte[] data, String fileName) throws FileUploadException;

	String uploadFile(byte[] data, String fileName, String filePath) throws FileUploadException;

}
