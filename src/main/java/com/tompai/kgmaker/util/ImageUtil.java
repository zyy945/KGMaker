/** 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tompai.kgmaker.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

public class ImageUtil {
	
	public static String getFilePath(String userCode)
	{
		return File.separator +userCode;
	}
    /**
    * 保存文件，直接以multipartFile形式
    * @param multipartFile
    * @param path 文件保存绝对路径
    * @return 返回文件名
    * @throws IOException
    */
   public static String saveImg(MultipartFile multipartFile,String path) throws IOException {
       File file = new File(path);
       if (!file.exists()) {
           file.mkdirs();
       }
       FileInputStream fileInputStream = (FileInputStream) multipartFile.getInputStream();
       String fileName = UuidUtil.getUUID() + ".png";
       BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(path + File.separator + fileName));
       byte[] bs = new byte[1024];
       int len;
       while ((len = fileInputStream.read(bs)) != -1) {
           bos.write(bs, 0, len);
       }
       bos.flush();
       bos.close();
       return fileName;
   }
}
