/** 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tompai.kgmaker.util;

import java.util.concurrent.TimeUnit;

import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Config;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Neo4jConfig {
	@Value("${spring.neo4j.url}")
	private String url;

	@Value("${spring.neo4j.username}")
	private String username;

	@Value("${spring.neo4j.password}")
	private String password;

	/**
	 * 图数据库驱动模式
	 *
	 * @return
	 */

	@Bean
	public Driver neo4jDriver() {
		Config config = Config.builder().withMaxConnectionLifetime(30, TimeUnit.MINUTES).withMaxConnectionPoolSize(50)
				.withConnectionAcquisitionTimeout(2, TimeUnit.MINUTES).withConnectionTimeout(15, TimeUnit.SECONDS)
				.withMaxTransactionRetryTime(15, TimeUnit.SECONDS).build();
		return GraphDatabase.driver(url, AuthTokens.basic(username, password),config);
	}

}