# KGMaker

#### 介绍
知识图谱可视化构建工具

#### 软件架构
这是一个知识图谱构建通用的小工具,可以在前端手动构建自己的知识图谱数据库，核心数据存储于Neo4j图数据库;


#### 安装教程

1. 前台是基于vue.js,d3.js等JavaScript可视化库
2. 后台是springboot2.x
3. 数据存储Neo4j3.x

#### 实现的基本功能:

1. 新增节点,添加连线,快速添加节点和关系
2. 节点的颜色和大小可修改
3. 节点和关系的编辑,删除
4. 导出成图片PNG
5. CSV文件导入
6. 导出数据成CSV文件
7. 添加图片和富文本
8. 节点之间多个关系

## 联系作者
- [oschina](http://git.oschina.net/liinux)
- [cnblogs](http://www.cnblogs.com/liinux)
- [github](https://github.com/liinnux)